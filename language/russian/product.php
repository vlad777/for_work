<?php
$_['title'] = 'Test';
$_['all_prod'] = 'All Products';
$_['one_prod'] = 'One Products';

$_['text_action'] = 'Action';
$_['text_delete'] = 'Delete';
$_['text_edit'] = 'Edit';

$_['prod_name'] = 'Product name';
$_['prod_price'] = 'Product price';

$_['heading_title'] = 'Form';
$_['button_save'] = 'Save';
$_['tab_general'] = 'common';
$_['text_name'] = 'name';
$_['text_special'] = 'spesials';
$_['entry_name'] = 'Entry name';
$_['tab_data'] = 'All data';
$_['tab_special'] = 'Spesial Prices';
$_['button_remove'] = 'Delete';
$_['entry_price'] = 'Entry price';
$_['entry_date_start'] = 'Entry date start';
$_['entry_date_end'] = 'Entry date end';
$_['button_add_special'] = 'Add';
$_['text_enabled'] = 'ON';
$_['text_disabled'] = 'OFF';
$_['entry_status'] = 'Status';
$_['entry_price'] = 'Entry price';