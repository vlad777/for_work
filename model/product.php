<?php 
class Model extends Controller {
    public function getALL(){
       return  $this->db->query("SELECT p.product_id, p.price, pd.name, (SELECT ps.price
            FROM  `" . DB_PREFIX."product_special` ps WHERE ps.product_id = p.product_id AND ( date_end - date_start ) = ( 
            SELECT MIN( date_end - date_start ) FROM  `" . DB_PREFIX."product_special` ps1
            WHERE ps1.product_id = ps.product_id ) LIMIT 0,1) AS sp_price FROM " . DB_PREFIX."product p LEFT JOIN " . DB_PREFIX."product_description pd 
            ON (p.product_id = pd.product_id) GROUP BY p.product_id ORDER BY p.product_id")->rows;
        
    }
    public function save($data,$id){
        if((int)$id>0){
            $this->db->query("DELETE FROM ".DB_PREFIX."product_description WHERE product_id = ".(int)$id);
            $this->db->query("DELETE FROM ".DB_PREFIX."product_special WHERE product_id = ".(int)$id);
            $product_id = (int)$id;
            $this->db->query("UPDATE ".DB_PREFIX."product SET status = ".(int)$data['status'].", date_modified = NOW(), `price` = ".(float)$data['price']." WHERE product_id = ".(int)$product_id."");
        }else{
            $this->db->query("INSERT INTO ".DB_PREFIX."product SET status = ".(int)$data['status'].", `price` = ".(float)$data['price'].", date_added = NOW()");
            $product_id = $this->db->getLastId();
        }
        foreach($data['product_description'] as $key=>$des){
            $this->db->query("INSERT INTO ".DB_PREFIX."product_description SET language_id = ".(int)$key.", product_id = $product_id, `name` = '".$this->db->escape($des['name'])."'");
        }
        if(!empty($data['product_special'])){
            foreach($data['product_special'] as $sp){
                $this->db->query("INSERT INTO ".DB_PREFIX."product_special SET `price` = ".(float)$sp['price'].", product_id = $product_id, `date_start` = '".$this->db->escape($sp['date_start'])."', `date_end` = '".$this->db->escape($sp['date_end'])."'");
            }
        }
        
    }
    public function edit($id){
        $product = array();
        $product['main'] = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id = ".(int)$id)->row;
        $product['description'] = $this->db->query("SELECT * FROM ".DB_PREFIX."product_description WHERE product_id = ".(int)$id)->rows;
        $product['special'] = $this->db->query("SELECT * FROM ".DB_PREFIX."product_special WHERE product_id = ".(int)$id)->rows;
        return  $product;
    }
    public function delete($id){
        $this->db->query("DELETE FROM ".DB_PREFIX."product WHERE product_id = ".(int)$id);
        $this->db->query("DELETE FROM ".DB_PREFIX."product_description WHERE product_id = ".(int)$id);
        $this->db->query("DELETE FROM ".DB_PREFIX."product_special WHERE product_id = ".(int)$id);
    }
    public function getLangs(){
        return $this->db->query("SELECT * FROM ".DB_PREFIX."language")->rows;
    }
}
