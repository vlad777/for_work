<?php 
class Header extends Controller{
    public function retRes(){
        $param = $this->language->load('header');
        $param['link_all_prod'] = $this->link('all');
        $param['link_one_prod'] = $this->link('one');
	    return $this->template->fetch('header.tpl',$param);
    }  
}
