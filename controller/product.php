<?php 
class Product extends Controller{
    private $header;
    private $footer;
    private $result;
    private $model;
    public function retRes($params){
        require(DIR_MODEL . 'product.php');
        $this->model = new Model($this->all_data);
        $footer = new Footer($this->all_data);
        $header = new Header($this->all_data);
        $this->header = $header->retRes();
        $this->footer = $footer->retRes();
        $function = $this->chekRoute($params['route']);
        return $this->$function();
     }
    public function delete(){
        if(isset($_GET['p_r'])){
            $this->model->delete($_GET['p_r']);
        }
        header("location: " . $this->link('all'));
    }
    private function all(){
        
        $param = $this->language->load('product'); 
        $param['header'] = $this->header;
        $param['footer'] = $this->footer;
        $param['href_delete'] = $this->link('delete');
        $param['href_edit'] = $this->link('one');
        $param['products'] = $this->model->getALL();
        return $this->template->fetch('product_list.tpl',$param);
    }
    private function save(){
        if(isset($_POST)&&!empty($_POST)){
            if($this->validate()){
                $this->model->save($_POST, isset($_GET['p_r'])?$_GET['p_r']:0);
            }else{
                return $this->one($er = 1);
            }
        }
        header("location: " . $this->link('all'));
    }
    private function one($er = 0){
        $param = $this->language->load('product');
        $param['product_data'] = array();
        
        if(isset($_GET['p_r'])){
            $param['product_data'] = $this->model->edit($_GET['p_r']);
            $d_s = array();
            foreach($param['product_data']['description'] as $descr){
               $d_s[$descr['language_id']] = $descr;
            }
            $param['product_data']['description'] = $d_s;
        }
        
        $param['error'] = false;
        if($er){
            $param['error'] = true;
        }
        
        $param['status'] = isset($param['product_data']['main']['status'])?$param['product_data']['main']['status']:1;
        $param['prod_price'] = isset($param['product_data']['main']['price'])?$param['product_data']['main']['price']:'';
        
        if(!empty($_POST)){
           $param['status'] = $_POST['status'];
           $param['prod_price'] = $_POST['price'];
           $param['product_data']['description'] = $_POST['product_description'];
           $param['product_data']['special'] = $_POST['product_special'];
        }
        
        $param['languages'] = $this->model->getLangs();   
        $param['header'] = $this->header;
        $param['footer'] = $this->footer;
        $param['action'] = isset($_GET['p_r'])?$this->link('save').'?p_r='.$_GET['p_r']:$this->link('save');
        return $this->template->fetch('product_form.tpl',$param);
    }
    private function validate(){
        foreach($_POST['product_description'] as $val){
            if(trim($val['name'])==""){
                return false;
            }
        }
        if(isset($_POST['product_special'])){
            foreach($_POST['product_special'] as $k=>$val){
                foreach($_POST['product_special'] as $k2=>$val_2){
                    if($k!=$k2&&$val['date_start']==$val_2['date_start']&&$val['date_end']==$val_2['date_end']){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
