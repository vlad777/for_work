<?php 
class Controller {
    protected $all_data;
    protected $template;
    protected $db;
    protected $language;
    private $link_array = array('all','one','delete','save');
    public function __construct($data){
        $this->all_data = $data;
        $this->db = $data['db'];
        $this->template = $data['template'];
        $this->language = $data['language'];
    }
    protected function link($url){
        return HTTP_SERVER . $url;
    }
    protected function chekRoute($route){
        if(array_search($route, $this->link_array)===false){
            return 'all';
        }else{
            return $route;
        }
    }
}