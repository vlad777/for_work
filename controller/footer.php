<?php
class Footer extends Controller {   
    public function retRes(){
        $this->language->load('footer');
        $param['text'] = $this->language->get('bottom_text'); 
	    return $this->template->fetch('footer.tpl',$param);
    }
}
