<?php
// Version
require('config.php');
require(DIR_SYSTEM . 'startup.php');
require(DIR_CONTROLLER . 'controller.php');
require(DIR_CONTROLLER . 'footer.php');
require(DIR_CONTROLLER . 'header.php');
require(DIR_CONTROLLER . 'product.php');

$data['db'] = new DB(DB_DRIVER,DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
$data['template'] = new Template();
$data['language'] = new Language('english');
if(!isset($_GET['route'])){
    $way['route'] = 'all';
}else{
    $way['route'] = $_GET['route'];
}
$result = new Product($data);
echo $result->retRes($way);
?>