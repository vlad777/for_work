<?php
class Template {
	public function fetch($filename, $data) {
		$file = DIR_TEMPLATE . $filename;

		if (file_exists($file)) {
			extract($data);

			ob_start();

			include($file);

			$content = ob_get_clean();

			return $content;
		} else {
			trigger_error('Error: Could not load template ' . $file . '!');
			exit();				
		}
	}
}
?>