<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>

<link type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

</head>
<body>
<div id="container">
    <div id="header">
        <div id="menu">
    		<ul class="left">
    			<li><a href="<?php echo $link_all_prod; ?>" class="top"><?php echo $all_prod; ?></a></li>
    			<li><a href="<?php echo $link_one_prod; ?>" class="top"><?php echo $one_prod; ?></a>
    		</ul>
        </div>
</div>
