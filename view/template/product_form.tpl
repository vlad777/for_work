<?php echo $header; ?>
<div id="content">
	<div class="box">
		<div class="heading">
			<h1><?php echo $heading_title; ?></h1>
			<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a></div>
		</div>
        
		<div class="content">
        <?php if($error){ ?>
            <div class="error">
                You Have error in your form!!!
            </div>
        <?php } ?>
			<div id="tabs" class="htabs">
				<a href="#tab-general"><?php echo $tab_general; ?></a>
				<a href="#tab-data"><?php echo $tab_data; ?></a>
				<a href="#tab-special"><?php echo $tab_special; ?></a>
			</div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-general">
                    <div id="languages" class="htabs">
						<?php foreach ($languages as $language) { ?>
						<a href="#language<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></a>
						<?php } ?>
					</div>
					<?php
					foreach ($languages as $language) { ?>
					<div id="language<?php echo $language['language_id']; ?>">
						<table class="form">
							<tr>
								<td><span class="required">*</span> <?php echo $entry_name;?></td>
								<td><input class="name_to_keyword" type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_data['description'][$language['language_id']]) ? $product_data['description'][$language['language_id']]['name'] : ''; ?>" />
							</tr>
						</table>
					</div>
					<?php } ?>
				</div>
                <div id="tab-data">
                    <table class="form">
						<tr>
							<td><?php echo $entry_status; ?></td>
							<td><select name="status">
									<?php if ($status) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
                        <tr>
							<td><?php echo $entry_price; ?></td>
							<td>
                                <input name="price" value="<?php echo $prod_price; ?>" />
							</td>
						</tr>
                   </table> 
                </div>
                <div id="tab-special">
					<table id="special" class="list">
						<thead>
							<tr>
								<td class="right"><?php echo $entry_price; ?></td>
								<td class="left"><?php echo $entry_date_start; ?></td>
								<td class="left"><?php echo $entry_date_end; ?></td>
								<td></td>
							</tr>
						</thead>
						<?php $special_row = 0; ?>
						<?php if(isset($product_data['special'])) foreach ($product_data['special'] as $product_special) { ?>
						<tbody id="special-row<?php echo $special_row; ?>">
							<tr>
							    <td class="right"><input type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" /></td>
								<td class="left"><input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" class="date" /></td>
								<td class="left"><input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" class="date" /></td>
								<td class="left"><a onclick="$('#special-row<?php echo $special_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
							</tr>
						</tbody>
						<?php $special_row++; ?>
						<?php } ?>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td class="left"><a onclick="addSpecial();" class="button"><?php echo $button_add_special; ?></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
	       </form>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
	$('#languages a').tabs(); 
	$('#vtab-option a').tabs();

	var special_row = <?php echo (int)$special_row; ?>;

	function addSpecial() {
		html  = '<tbody id="special-row' + special_row + '">';
		html += '  <tr>'; 
		html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][price]" value="" /></td>';
		html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_start]" value="" class="date" /></td>';
		html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_end]" value="" class="date" /></td>';
		html += '    <td class="left"><a onclick="$(\'#special-row' + special_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
		html += '  </tr>';
		html += '</tbody>';
		
		$('#special tfoot').before(html);
	 
		$('#special-row' + special_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
		
		special_row++;
	}
//--></script> 

	<script type="text/javascript"><!--
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
	//--></script> 
<?php echo $footer; ?>