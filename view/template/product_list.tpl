<?php echo $header; ?>
<div id="content">
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
		<div class="content">
			<table class="list">
					<tr>
                        <td>№</td>
                        <td><?php echo $prod_name; ?></td>
                        <td><?php echo $prod_price; ?></td>
                        <td><?php echo $text_action; ?></td>
                    </tr>
                    <?php foreach($products as $key=>$prod){ ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $prod['name']; ?></td>
                        <td><?php echo empty($prod['sp_price'])?$prod['price']:$prod['sp_price']; ?></td>
                        <td><p><a href="<?php echo $href_delete.'?p_r='.$prod['product_id']; ?>"><?php echo $text_delete; ?></a></p>
                            <p><a href="<?php echo $href_edit.'?p_r='.$prod['product_id']; ?>" ><?php echo $text_edit; ?></a></p>
                        </td>
                    </tr>
                    <?php } ?>
			</table>
		</div>
	</div>
</div>
<?php echo $footer; ?>